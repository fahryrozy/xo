console.log('Check with String Match')
console.log('==========================')
function xo(str) {
  const check = str.match(/x/gi).length == str.match(/o/gi).length;
  return check;
}

// TEST CASES
console.log(xo('xoXoxo')); // true
console.log(xo('oxOoxo')); // false
console.log(xo('oxo')); // false
console.log(xo('xxxooo')); // true
console.log(xo('xoxooxxo')); // true

console.log('Check with Looping')
console.log('==========================')
function xo_for(str) {
  let xstr = [];
  let ostr = [];

  for(let i=0; i<str.length; i++) {
    if(str[i] == 'x' ) {
      xstr.push(str[i]);
    }
    else {
      ostr.push(str[i]);
    }
  }
  return (xstr.length == ostr.length) ? true : false
}

// TEST CASES
console.log(xo_for('xoxoxo')); // true
console.log(xo_for('oxooxo')); // false
console.log(xo_for('oxo')); // false
console.log(xo_for('xxxooo')); // true
console.log(xo_for('xoxooxxo')); // true